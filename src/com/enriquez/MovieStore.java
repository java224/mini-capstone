package com.enriquez;

public class MovieStore {
    private String storeName;
    private String location;

    public MovieStore(String newStoreName, String newLocation) {
        this.storeName = newStoreName;
        this.location = newLocation;
    }

    public String getStoreName() {
        return this.storeName;
    }

    public String getLocation() {
        return this.location;
    }

    public void setStoreName(String newStoreName) {
        this.storeName = newStoreName;
    }

    public void setLocation(String newLocation) {
        this.location = newLocation;
    }

    public String describe() {
        return this.getStoreName() + " " + this.getLocation() + " Branch";
    }
}
