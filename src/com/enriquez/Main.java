package com.enriquez;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        MovieStore VideoCity = new MovieStore("Video City", "Quezon");

        System.out.println("Welcome to " + VideoCity.describe() + "!");
        System.out.println("Please enter your name");
        Scanner scan = new Scanner(System.in);
        String user;

        user = scan.nextLine();
        System.out.println("Hello " + user );

        ArrayList<String>  movieList = new ArrayList<String>();
        Movie a = new Movie("1","a");
        movieList.add(a.describe());
        Movie b = new Movie("2","a");
        movieList.add(b.describe());
        Movie c = new Movie("3","a");
        movieList.add(c.describe());
        Movie d = new Movie("4","d");
        movieList.add(d.describe());
        Movie e = new Movie("5","e");
        movieList.add(e.describe());
        Movie f = new Movie("6","f");
        movieList.add(f.describe());

        System.out.println("What do you want to do?");
        System.out.println("Commands: Add = Add Movie/Category // Browse = Search Movie/Category // Print All = Print all Movies");

        String command;
        command = scan.nextLine();
        if (command.equals("Add")) {
            String YN = "Y";
            while (YN.equals("Y")) {
                String name;
                String category;

                System.out.println("Please Enter Movie Name");
                name = scan.nextLine();
                System.out.println("Please Enter Movie Category");
                category = scan.nextLine();
                String data =   name + " = " + category;
                movieList.add(data);
                System.out.println(data + " has been Added");
                System.out.println("Do you want to Add another Yes or No ** Command Y = yes || N = no **");
                YN = scan.nextLine();
                if (YN.equals("N")){
                Main.main(args);
                }
            }
        } else if (command.equals("Browse")){
            System.out.println("Search");
            String YN = "Y";
            while (YN.equals("Y")) {
                String browse;

                System.out.println("Please Enter Movie Name/Category to Search");
                browse = scan.nextLine();
                for (String movie : movieList) {
                    if (movie.contains(browse)) {
                        System.out.println(movie);
                    }

                    System.out.println("Do you want to Search another Movie Yes or No? ** Command Y = yes || N = no **");
                    YN = scan.nextLine();
                    if (YN.equals("N")) {
                        Main.main(args);
                    }
                }
            }
        } else if(command.equals("Print")){
            System.out.println("Print");
            String YN = "Y";
            while (YN.equals("N")) {

                System.out.println("Please Enter Movie Name/Category to Search");
                for (String movie : movieList) {
                        System.out.println(movie);
                    }
                    System.out.println( "Leave Yes or No? ** Command Y = yes || N = no **");
                    YN = scan.nextLine();
                    if (YN.equals("Y")) {
                        Main.main(args);
                    }
                }
        } else {
            System.out.println("Command not Available");
            Main.main(args);
                }
            }
}


