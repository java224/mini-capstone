package com.enriquez;

public class Movie {

    private String name;
    private String category;

    public Movie(String newName, String newCategory){
        this.name = newName;
        this.category = newCategory;
    }

    public String getName(){
        return this.name;
    }
    public String getCategory(){
        return this.category;
    }

    public void setName(String newName){
        this.name = newName;
    }
    public void setCategory(String newCategory){
        this.category = newCategory;
    }
    public String describe(){
        return   this.getName() + " = " + this.getCategory();
    }
}

